#!/bin/bash

echo "Listando as regras de redirecionamento existentes: "
echo " "

iptables --line-numbers -t nat -L


echo "Qual regra deseja excluir? "
read NUME

echo " "

echo "Excluindo a regra de número $NUME."

iptables -t nat -D PREROUTING $NUME


echo " "

echo "Regra excluída com sucesso!!"

